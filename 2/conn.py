import socket
import os
import time

class ConnectError(Exception):
	def __init__(self, str):
		self.message = str

class Client:
	
	# Size of part of data
	__clus = 512 * 1024
	
	def __init__(self, ip, port, timeout = 0):
		self.__ip = ip
		self.__port = port
		self.__timeout = timeout
	
	# Check valid file
	def __checkfile(self, file):
		if not os.path.exists(file):
			raise ConnectError("File not exists")
		if not os.path.isfile(file):
			raise ConnectError("File is not file")
	
	# Get binary header of file
	def __getbheader(self, file):
		bname = file.encode("utf-8")
		namesize = len(bname)
		if namesize > 4096:
			raise ConnectError("Big name of file")
		bnamesize = namesize.to_bytes(2, byteorder = "big")
		fileinfo = os.stat(file)
		filesize = fileinfo.st_size
		if filesize > 1024 * 1024 * 1024 * 1024:
			raise ConnectError("Bit file size")
		bfilesize = filesize.to_bytes(6, byteorder = "big")
		bheader = bnamesize + bname + bfilesize
		return bheader
	
	# Get client socket
	def __getsocket(self):
		s = socket.socket()
		try:
			s.connect((self.__ip, self.__port))
		except ConnectionRefusedError:
			raise ConnectError("Cannot connect")
		s.settimeout(self.__timeout)
		return s
	
	def __getspeed(self, speed):
		if(speed < 1000):
			return "[%10d] bits/s" % speed
		if(speed < 1000*1000):
			return "[%10d] Kbits/s" % (speed/1000)
		return "[%10d] Mbits/s" % (speed/1000/1000)
	
	# Read file to client socket
	def __file2socket(self, f, s):
		with open(f, "rb") as fil:
			t = time.time()
			while True:
				buf = fil.read(self.__clus)
				if buf == None or len(buf) == 0:
					break
				try:
					t = time.time()
					s.sendall(buf)
					t = time.time() - t
				except socket.timeout:
					raise ConnectError('Sending timeout')
				except BrokenPipeError:
					raise ConnectError('Pipe is broken')
				except ConnectionResetError:
					raise ConnectError('Connection is reset')
				speed = len(buf) * 8 / t
				print(self.__getspeed(speed))
		pass
	
	# Read part
	def __read(self, s, size):
		res = b""
		while len(res) < size:
			try:
				buf = s.recv(size - len(res))
			except socket.timeout:
				raise ConnectError("Read timeout")
			if buf == None or len(buf) == 0:
				if len(res) < size:
					raise ConnectError("Unexpected end of read")
				break
			res += buf
		return res
	
	# Bytes to long
	def __parselong(self, bytes):
		return int.from_bytes(bytes, byteorder = "big")
	
	# Read str
	def __getstr(self, s):
		len = self.__parselong(self.__read(s, 4))
		bstr = self.__read(s, len)
		try:
			return bstr.decode("utf-8")
		except UnicodeDecodeError:
			raise ConnectError("Invalide response")
	
	# Send data
	def __senddata(self, s, data):
		try:
			s.sendall(data)
		except BrokenPipeError:
			raise ConnectError("Cannot send data")
		pass
	
	# Full stack
	def send(self, file):
		self.__checkfile(file)
		s = self.__getsocket()
		self.__senddata(s, self.__getbheader(file))
		err = None
		try:
			self.__file2socket(file, s)
		except ConnectError as e:
			err = e
		try:
			res = self.__getstr(s)
			s.close()
			return res
		except ConnectError as e:
			if err != None:
				raise err
			else:
				raise e

# -------------------------------------------------------------------------------------------------------------------------------------------

class Server:
	
	# Size of part of data
	__clus = 512 * 1024
	
	# Static folder
	__static = "static"
	
	def __init__(self, ip, port, timeout = 0):
		self.__ip = ip
		self.__port = port
		self.__timeout = timeout
		self.__socket = None
	
	# Bind socket
	def bind(self, count):
		s = socket.socket()
		try:
			s.bind((self.__ip, self.__port))
		except OSError as e:
			raise ConnectError("Cannot bind: " + str(e))
		s.settimeout(self.__timeout)
		s.listen(count)
		self.__socket = s
	
	# Get connection or None if timeout
	def getSocket(self):
		s = self.__socket
		if s == None:
			raise ConnectError("Firstly bind the socket")
		try:
			con, ip = s.accept()
		except socket.timeout:
			return (None, None)
		con.settimeout(self.__timeout)
		return (con, ip)
	
	# Read part
	def __read(s, size):
		res = b""
		while len(res) < size:
			try:
				buf = s.recv(size - len(res))
			except socket.timeout:
				raise ConnectError("Read timeout")
			if buf == None or len(buf) == 0:
				if len(res) < size:
					raise ConnectError("Unexpected end of read")
				break
			res += buf
		return res
	
	# Bytes to long
	def __parselong(bytes):
		return int.from_bytes(bytes, byteorder = "big")
	
	# Read name
	def __readnamesize(s):
		bnamesize = Server.__read(s, 2)
		namesize = Server.__parselong(bnamesize)
		bname = Server.__read(s, namesize)
		name = bname.decode("utf-8")
		bsize = Server.__read(s, 6)
		size = Server.__parselong(bsize)
		return (name, size)
	
	# Read file
	def __read2file(s, f, size):
		readed = 0
		with open(f, "wb") as fil:
			while readed < size:
				buf = s.recv(size - readed)
				if buf == None or len(buf) == 0:
					raise ConnectError("Unexpected end")
				readed += len(buf)
				fil.write(buf)
		pass
	
	# Get valid name
	def __getvalidname(name):
		dir, name = os.path.split(name)
		name = os.path.join(Server.__static, name)
		if os.path.exists(name):
			raise ConnectError("File already exists")
		return name
	
	# Match data
	def __gosocket(s):
		name, size = Server.__readnamesize(s)
		name = Server.__getvalidname(name)
		Server.__read2file(s, name, size)
	
	# Send result
	def __writestr(s, str):
		bstr = str.encode("utf-8")
		strlen = len(bstr)
		bstrlen = strlen.to_bytes(4, byteorder = "big")
		s.sendall(bstrlen + bstr)
	
	# Full stack
	def goSocket(s):
		try:
			Server.__gosocket(s)
			res = "Sucs"
		except ConnectError as e:
			res = e.message
		Server.__writestr(s, res)
		s.close()
	
	def exit(self):
		self.__socket.close()
